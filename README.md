# Trang Giới Thiệu #

Đây chỉ là trang giới thiệu thôi nha các bạn . Cái này không quá quan trong đâu.

### Thông Tin Nhóm  ###

* [FaceBook](https://www.facebook.com/groups/1466254043393606/)
* [Google Docs](https://docs.google.com/document/d/1iYKDPrLN2ua8ZcAOUGOYkg00H66_kF_QnF9UHr0Bp-U/edit)
* [Tại Liệu Học Tập ](https://www.dropbox.com/sh/d2o9grm8c1kdbgx/AABSfD7QyVsD6LE-F5zLAevxa?dl=0)

### Thông Tin Thành Viên ? ###

* [Project Manager](https://www.facebook.com/tranthaison.duong?fref=nf)
* [Technical Leader](https://www.facebook.com/profile.php?id=100007263221039)
* [Technical Write](https://www.facebook.com/nguyen.h.nguyen.56)
* [Developer](https://www.facebook.com/ngothanhphi1412)
* [Developer](https://www.facebook.com/profile.php?id=100008284727720)
* [Tester](https://www.facebook.com/tuanhai.dinhvan)

### Các Mục Khác Sẽ Xem Xét Bost lên Sau ###

* Writing tests
* Code review
* Other guidelines

### Phương Châm ###

* Đoàn Kết, Hết Lòng, Thành Công